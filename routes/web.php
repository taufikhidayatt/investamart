<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/page-1', function () {
    return view('page-1');
});

Route::get('/page-2', function () {
    return view('page-2');
});

Route::get('/page-3', function () {
    return view('page-3');
});

Route::get('/page-4', function () {
    return view('page-4');
});

Route::get('/page-5', function () {
    return view('page-5');
});

Route::get('/page-6', function () {
    return view('page-6');
});

Route::get('/page-7', function () {
    return view('page-7');
});

Route::get('/page-8', function () {
    return view('page-8');
});

Route::get('/page-9', function () {
    return view('page-9');
});

Route::get('/page-10', function () {
    return view('page-10');
});

Route::get('/result', function () {
    return view('result');
});
