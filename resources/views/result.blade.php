@extends('master')

@section('content')
<body class="backgroundBlue">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-7 col-md-8 mx-lg-auto mx-md-auto">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <img style="height: 70px;" src="{{ asset('/images/logo.svg') }}" alt="" srcset="">
                    </div>
                    <div class="col-lg-12 text-center">
                        <img class="w-100" src="{{ asset('/images/logo-result.svg') }}" alt="" srcset="">
                    </div>
                    <div class="col-lg-12 mt-4 text-center kalimatTanya text-white">
                        <h6>KAMU ADALAH</h6>
                        <h3>THE OPPORTUNIST</h3>
                        <h6>A.K.A KONSERVATIF</h6>
                    </div>
                    <div class="col-lg-12 mt-4 text-center kalimatHome text-white">
                        <p>
                        Kamu memiliki toleransi terhadap resiko yang cenderung rendah dimana return yang diharapkan minimal setara dengan suku bunga deposito dengan fluktuasi nilai pasar yang minimal untuk mencapai tujuan investasi kamu.
                        </p>
                    </div>
                    <div class="col-lg-12 mt-4 text-center kalimatTanya text-white">
                        <h6>POTENSI KEUANGAN KAMU 10 TAHUN MENDATANG</h6>
                        <img class="w-100" src="{{ asset('/images/chart.svg') }}" alt="" srcset="">
                    </div>
                    <div class="col-lg-12 mt-4 text-center kalimatHome text-white">
                        <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                    </div>
                    <div class="col-lg-12 col-md-12 col-12 text-center">
                        <a href="{{ url('/') }}" class="btn btn-black">SHARE ></a>
                    </div>
                    <div class="col-lg-12 mt-4 text-center kalimatTanya text-yellow">
                        <h5>
                            AYO MULAI BERINVESTASI DARI SEKARANG
                            DAN DAPATKAN BONUS  DIAWAL SEBESAR
                            IDR 1,000,000 REKSADANA TUNAI
                        </h5>
                    </div>
                    <div class="col-lg-12 mt-2 text-center kalimatHome text-white">
                        <p>
                        Cukup dengan mendaftarkan diri kamu dengan mengisi form dibawah ini, kamu akan mendapatkan bonus langsung reksadana di aplikasi Invesnow!
                        </p>
                    </div>
                    <div class="col-lg-12 mt-2 text-left kalimatTanya">
                        <input class="form-control form-control-sm mb-2" type="text" placeholder="NAMA">
                        <input class="form-control form-control-sm mb-2" type="email" placeholder="EMAIL">
                        <input class="form-control form-control-sm mb-2" type="text" placeholder="MOBILE">
                    </div>
                    <div class="col-lg-12 col-md-12 col-12 mb-3 text-right">
                        <a href="{{ url('/') }}" class="btn btn-black">SUBMIT</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
@endsection