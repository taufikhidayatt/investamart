@extends('master')

@section('content')
<body class="backgroundOrange">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-7 col-md-8 mx-lg-auto mx-md-auto">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <img style="height: 70px;" src="{{ asset('/images/logo.svg') }}" alt="" srcset="">
                    </div>
                    <div class="col-lg-12 text-center">
                        <img style="height: 110px;" src="{{ asset('/images/logo-2.svg') }}" alt="" srcset="">
                    </div>
                    <div class="col-lg-12 mt-4 text-center kalimatTanya">
                        <h3>BERAPA LAMA <br> WAKTU YANG <br> DIBUTUHKAN <br> UNTUK MENCAPAI <br> GOAL KAMU?</h3>
                    </div>
                    <div class="col-lg-12 mt-2 px-3 text-left kalimatTanya">
                        <ol>
                            <li>1-2 TAHUN</li>
                            <li>> 3-5 TAHUN</li>
                        </ol>
                    </div>
                    <div class="col-lg-12 mt-3 mb-3 text-center">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-4 pr-0">
                                <a href="{{ url('/page-1') }}" class="btn btn-black">< ULANG</a>
                            </div>
                            <div class="col-lg-4 col-md-4 col-4 text-center pageHalaman">
                                <h5>
                                    PERTANYAAN
                                    <br>
                                    2/10
                                </h5>
                            </div>
                            <div class="col-lg-4 col-md-4 col-4 pl-0">
                                <a href="{{ url('/page-3') }}" class="btn btn-black">LANJUT ></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
@endsection