@extends('master')

@section('content')
<body class="backgroundOrange">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-7 col-md-8 mx-lg-auto mx-md-auto">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <img style="height: 70px;" src="{{ asset('/images/logo.svg') }}" alt="" srcset="">
                    </div>
                    <div class="col-lg-12 text-center">
                        <img style="width:85%" src="{{ asset('/images/home-image.svg') }}" alt="" srcset="">
                    </div>
                    <div class="col-lg-12 mt-2 px-4 text-center kalimatHome">
                    <p> 
                    Penjelasan tentang apa itu Profil Resiko Gue dan iming2 hadiah. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </p>
                    </div>
                    <div class="col-lg-12 text-center mb-3">
                        <a href="{{ url('/page-1') }}" class="btn btn-black">Mulai Sekarang ></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
@endsection